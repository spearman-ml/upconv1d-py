#!/usr/bin/env python3

import os

import numpy as np
import tensorflow as tf

print ("upconv1d main...")

# suppress info messages from .cc source files
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "1"

#tf.compat.v1.enable_eager_execution()
tf.compat.v1.disable_eager_execution()

print ("tensorflow version: ", tf.version.VERSION)
print ("eager execution:    ", tf.executing_eagerly())
np.set_printoptions (linewidth=200)

input_dimension = 5
output_dimension = 5

inputs   = np.array ([10.0, 1.0, -1.0, 3.0, 4.0],
  dtype='float32').reshape ((1, input_dimension, 1))
print ("inputs: ", inputs)
input1   = tf.compat.v1.placeholder ('float32', shape=(1, input_dimension, 1))
upconv1d = np.array ([1.0/2.0, 1.0/3.0, 1.0/4.0],
  dtype='float32').reshape ((3, 1, 1))
print ("filter: ", upconv1d)
filters1 = tf.Variable (tf.constant (upconv1d))
out1     = tf.nn.conv1d (input1, filters1, stride=2, padding='SAME')
print ("out1 shape:", out1.shape)
out2     = tf.nn.conv1d_transpose (out1, filters1, [1, output_dimension, 1], 2)
out2b    = tf.nn.conv1d_transpose (out1, filters1, [1, 3, 1], 1)
out2c    = tf.nn.conv1d (out1, filters1, stride=None, padding='SAME')

# run session
with tf.compat.v1.Session() as sess:
  init = tf.compat.v1.global_variables_initializer()
  feed_dict = { input1: inputs }
  print ("init: ", sess.run (init))

  print ("running conv1d layer...")
  result1 = sess.run (out1, feed_dict)
  print ("result1: ", result1.shape, '\n', result1)
  result2 = sess.run (out2, feed_dict)
  print ("result2: ", result2.shape, '\n', result2)
  result2b = sess.run (out2b, feed_dict)
  print ("result2b: ", result2b.shape, '\n', result2b)
  result2c = sess.run (out2c, feed_dict)
  print ("result2c: ", result2c.shape, '\n', result2c)

print ("...upconv1d main")
